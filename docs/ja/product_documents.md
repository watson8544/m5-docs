# 製品ドキュメント

[中文](zh_CN/product_documents) | [English](/en/product_documents) | 日本語

## コア

<img src='assets/img/product_pics/1.jpg'><img src='assets/img/product_pics/cores.png'>

| M5Core | MiniCore |
|:------:|:--------:|
| [BASIC](ja/product_documents/m5stack-core/m5core_basic) | [Stick](ja/product_documents/m5stack-core/minicore_stick) |
| [GRAY](ja/product_documents/m5stack-core/m5core_gray)   | / |
| [FIRE](ja/product_documents/m5stack-core/m5core_fire)   | / |

## モジュール

<img src='assets/img/product_pics/2.jpg'><img src='assets/img/product_pics/module.png'>

| 無線通信モジュール | アクセサリモジュール | 制御モジュール |
|:---------------:|:-----------------:|:-----------:|
| [GPS](ja/product_documents/modules/module_gps) | [PROTO](ja/product_documents/modules/module_proto) | [STEPMOTOR](ja/product_documents/modules/module_stepmotor) |
| [LORA](ja/product_documents/modules/module_lora) | [BATTERY](ja/product_documents/modules/module_battery) | [Node](ja/product_documents/modules/module_node) |
| [SIM800/GPRS/GSM](ja/product_documents/modules/module_sim800) | [BTC](ja/product_documents/modules/module_btc) | / |
| / | [PLUS](ja/product_documents/modules/module_plus) | / |

## ベース

<img src='assets/img/product_pics/5.jpg'><img src='assets/img/product_pics/bases.png'>

- [M5GO](ja/product_documents/bases/m5go_base)
- [PLC](ja/product_documents/bases/plc_base)
- [FACES](ja/product_documents/bases/face_base)
- [LAN](ja/product_documents/bases/lan_base)

## アクセサリ

<img src='assets/img/product_pics/5.jpg'><img src='assets/img/product_pics/accessory.png'>

- [LEGO-CABLE](ja/product_documents/accessories/cables/lego_cable)
- [GROVE-CABLE](ja/product_documents/accessories/cables/grove_cable)
- [FRAME](ja/product_documents/accessories/frame)
- [Headers Socket](ja/product_documents/accessories/headers_socket)
- [USB-TTL UART Serial Adapter](ja/product_documents/accessories/usb_uart_adapter)

## ユニット

<img src='assets/img/product_pics/3.jpg'><img src='assets/img/product_pics/unit.png'>

| 入力/センシング | 出力/コントロール | インターフェース |
|:-------------:|:--------------:|:-------------:|
| [ENV](ja/product_documents/units/unit_env) | [RGB](ja/product_documents/units/unit_rgb) | [HUB](ja/product_documents/units/unit_hub) |
| [IR](ja/product_documents/units/unit_ir) | [RELAY](ja/product_documents/units/unit_relay) | [3.96PORT](ja/product_documents/units/unit_396port) |
| [PIR](ja/product_documents/units/unit_pir)                   | [NeoPixel](ja/product_documents/units/unit_neopixel) | / |
| [ANGLE](ja/product_documents/units/unit_angle)               | [ESP32Cam](ja/product_documents/units/unit_esp32cam) | / |
| [EARTH](ja/product_documents/units/unit_earth)            | [M5Camera](ja/product_documents/units/unit_m5camera) | / |
| [LIGHT](ja/product_documents/units/unit_light)               | / | / |
| [MAKEY](ja/product_documents/units/unit_makey)               | / | / |
| [BUTTON](ja/product_documents/units/unit_button)             | / | / |
| [Dual BUTTON](ja/product_documents/units/unit_dual_button)   | / | / |
| [JOYSTICK](ja/product_documents/units/unit_joystick)         | / | / |
| [THERMAL](ja/product_documents/units/unit_thermal)           | / | / |
| [ADC](ja/product_documents/units/unit_ADC)                   | / | / |
| [DAC](ja/product_documents/units/unit_DAC)                   | / | / |
| [COLOR SENSOR](ja/product_documents/units/unit_color_sensor) | / | / |
| [ToF](ja/product_documents/units/unit_tof)                   | / | / |
| [NCIR](ja/product_documents/units/unit_pir)                  | / | / |

## アプリケーション

<img src='assets/img/product_pics/4.jpg'><img src='assets/img/product_pics/application.png'>

- [BALA](ja/product_documents/applications/application_bala)
- [Lidar BOT](ja/product_documents/applications/application_lidarbot)

## ツール

<img src='assets/img/product_pics/6.jpg'><img src='assets/img/product_pics/tool.png'>

- [M5Stack USB Downloader](ja/product_documents/tools/tool_usb_downloader)