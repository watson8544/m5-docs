# M5Stack 公式ドキュメント

[![Codacy grade](https://img.shields.io/codacy/grade/860d40719cbd4e0f91e145b87ec7c29a.svg?style=flat-square)](https://www.codacy.com/app/watson8544/M5Stack-Documentation-docsify?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=watson8544/M5Stack-Documentation-docsify&amp;utm_campaign=Badge_Grade)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square)](https://github.com/watson8544/M5Stack-Documentation-docsify/blob/master/LICENSE)
[![Tweet](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)](https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2Fjhildenbiddle%2Fdocsify-themeable&hashtags=css,docsify,developers,frontend)
<a class="github-button" href="https://github.com/m5stack/m5-docs" data-icon="octicon-star" data-show-count="true" aria-label="Star M5Stack/m5-docs on GitHub">Star</a>

## **M5Stackの世界へようこそ!**

<!-- <figure class="thumbnails">
    <img src="assets/img/m5stack.png" alt="Screenshot of coverpage" title="Cover page">
</figure> -->

## 製品紹介

**M5Stackはモジュール積層型の開発デバイスです。M5Stackの名前はModuleのM、5x5cmサイズの5、積み重ねるを意味するStackからきています。**

**M5Stackは、開発の際の煩わしさやストレスを軽減し、あなたがあなた自身の新しい発明やアイデアを具現化することに集中出来るように設計されています。M5Stackはいくつかのセンサー、Wi-Fi、Bluetooth、スクリーン、電源を自身に備えており、従来のブレッドボードでの煩雑な配線作業を行わなくても、拡張モジュールを積み重ねたり、Groveコネクタを介して拡張ユニットを簡単に接続することが可能です。**

M5Stack Coreモジュールは **ESP32** チップをベースに開発されています。

私たちの目的は、**<mark>誰もが簡単に自分のアイデアを形にできるような仕組みを作る</mark>**ことです。

オフィシャルサイトは [こちら](www.m5stack.com)です。

<!-- <figure class="thumbnails">
    <img src="assets/img/transport.gif" alt="Screenshot of coverpage" title="Cover page">
</figure> -->

|<img src="assets/img/introduction_pics/product-documents.jpg"> | <img src="assets/img/introduction_pics/m5-awesome.jpg"> |
|:---:|:---:|
|[製品ドキュメント](ja/product_documents) | [M5Stack 応用例](ja/m5stack_cases) |

|<img src="assets/img/introduction_pics/m5-api-reference.jpg"> | <img src="assets/img/introduction_pics/FAQ.jpg">|
|:---:|:---:|
|[API リファレンス](ja/api_reference) | [よくある質問](ja/faq)|

<!--
|<img src="https://github.com/m5stack/m5-docs/tree/master/docs/assets/img/introduction_pics/product-documents.jpg"> | <img src="https://github.com/m5stack/m5-docs/tree/master/docs/assets/img/introduction_pics/m5-awesome.jpg"> |
|:---:|:---:|
|[製品ドキュメント](ja/product_documents_ja) | [M5Stack 応用例](ja/m5stack_cases_ja) |

|<img src="https://github.com/m5stack/m5-docs/tree/master/docs/assets/img/introduction_pics/m5-api-reference.jpg"> | <img src="https://github.com/m5stack/m5-docs/tree/master/docs/assets/img/introduction_pics/FAQ.jpg">|
|:---:|:---:|
|[API リファレンス](ja/api_reference_ja) | [よくある質問](ja/faq_ja)| -->

## コンタクト&サポート

- :computer: 最新情報は[公式サイト](http://www.m5stack.com)(英語)
- :busts_in_silhouette: [フォーラム](http://forum.m5stack.com)にサインインしてM5Stackの情報交換をしよう
- :mailbox_with_mail: 質問・疑問は[Email: tech@m5stack.com](mailto:tech@m5stack.com)まで
- :convenience_store: 購入は[スイッチサイエンス](https://www.switch-science.com/catalog/list/770/)または[AliExpress](https://www.aliexpress.com/store/3226069)から

## ライセンス

このプロジェクトのライセンスは[MIT license](https://github.com/watson8544/M5Stack-Documentation-docsify/blob/master/LICENSE)です。

Copyright (c) 2018 M5Stack ([@M5Stack](https://twitter.com/M5Stack))

<!-- GitHub Buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
