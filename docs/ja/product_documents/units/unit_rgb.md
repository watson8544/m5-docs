# RGB ユニット

## 概要

RGB ユニットは3つのフルカラーLEDを備えており、指定した色を表示させる事が可能です。2つのGroveインターフェースを備えています。

## 特徴

- 3x フルカラーLED
- LEGO 互換ホール

## ドキュメント

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_rgb.png">
</figure>

## 関連情報

- [RGB ユニット購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-RGB-Unit-with-NeoPixel-RGB-LED-Light-x3-GPIO-GROVE-Connector/3226069_32929809133.html)