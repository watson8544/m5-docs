# THERMAL ユニット

## 概要

THERMAL ユニットはサーモパイルセンサー**MLX90640**を備えたサーモグラフィックカメラユニットです。解像度は32x64ピクセルです。遠くの物体の表面温度を±1.5°Cの精度で計測する事が可能です。I2Cで値を取得できます。

## 特徴

- 解像度: 32x24 pixels
- 測定可能範囲: -40°C ÷ 300°C
- Grove インターフェース
- LEGO 互換ホール

## アプリケーション

- 高精度非接触温度測定器
- 侵入 / 移動検知
- サーモグラフィ
- ビルのスマート空調システム

## ドキュメント

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Modules/MLX90640)
  - [Arduino MLX90640 制御](https://github.com/melexis/mlx90640-library)

- データシート
  - [MLX90640](https://www.melexis.com/-/media/files/documents/datasheets/mlx90640-datasheet-melexis.pdf)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_thermal.png" height="300" width="300">
</figure>

## 関連情報

- [THERMAL ユニット 購入(AliExpress)](https://ja.aliexpress.com/store/product/M5Stack-Official-New-Thermal-Camera-MLX90640-with-GROVE-I2C-Compatible-M5GO-FIRE-ESP32-Kit-Mini-Development/3226069_32918177644.html)