# ToF ユニット

## 概要

This is a unit can detect distance using a newest "Time-to-Flight" sensor using laser light. It is higher precision than most distance sensors. The unit comunicates with M5Core with I2C.

ToF ユニットはレーザー光を使った最新の「Time-to-Flight」センサーを使用して距離を検出できるユニットです。 ほとんどの距離センサーよりも高い精度で計測可能です。I2Cで値を取得可能です。

## 特徴

- 高精度
- 測定可能距離 2m
- LEGO 互換ホール

## アプリケーション

- 1次元ジェスチャー認識

## ドキュメント

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/TOF_VL53L0X)
- データシート
  - [VL53L0X](https://pdf1.alldatasheet.com/datasheet-pdf/view/948120/STMICROELECTRONICS/VL53L0X.html)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_tof.png" height="300" width="300">
</figure>

## 関連情報

- [ToF ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-ToF-Unit-VL53L0X-Time-of-Flight-ToF-Laser-Ranging-Sensor-Breakout-Laser-Distance-Sensor/3226069_32949310300.html)