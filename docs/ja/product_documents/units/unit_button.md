# BUTTON ユニット

## 概要

BUTTON ユニットは一つのボタンを備えたユニットです。

## 特徴

- LEGO 互換ホール
- Grove インターフェース

## ドキュメント

- ウェブサイト
  - [M5Stack](https://www.m5stack.com)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_button.png">
</figure>

## 関連情報

- [BUTTON ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-Button-Unit-for-ESP32-Arduino-Micropython-Development-Kit-with-GROVE-GPIO-Port-Blockly/3226069_32921805637.html)