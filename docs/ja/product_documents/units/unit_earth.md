# EARTH ユニット

## 概要

EARTH ユニットは土壌に含まれる水分量を検出する事ができるユニットです。土壌中の水分量をアナログ値として取得できるほか、水分量が一定値を超えているかどうかをデジタル値(0/1)として取得可能です。

## 特徴

- 10KΩ調整抵抗による閾値調整可能
- アナログ & デジタル出力
- Grove インターフェース
- LEGO 互換ホール

## ドキュメント

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Earth)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_moisture.jpg" height="300" width="300">
</figure>

## 関連情報

- [EARTH ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Earth-Module-Grove-Compatible-Soil-monitoring-Analog-and-Digital-Output/3226069_32922643696.html)