# ENV ユニット

## 概要

ENV ユニットは周辺の温度・湿度・気圧を取得する事ができます。

## 特徴

- 温度:
  - 計測範囲: 20 ~ 60℃
  - 精度: ±0.2℃
- 湿度:
  - 計測範囲: 20 ~ 95℃
  - 精度: 0.1%
- 気圧:
  - 計測範囲: 300 ~ 1100hPa
  - 精度: ±1hPa
- LEGO 互換ホール

## ドキュメント

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_env.png">
</figure>

## 関連情報

- [ENV ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-ENV-Unit-with-DHT12-BMP280-Digital-DHT-12-Temperature-Humidity-Aire-Pressure-Sensor/3226069_32933115893.html)