# LIGHT ユニット

## 概要

LIGHT ユニットは調整抵抗付き明るさセンサーです。明るさをアナログ値として、光のオンオフをデジタル値(0/1)として取得できます。

## 特徴

- 10KΩ調整抵抗による閾値調整可能
- アナログ & デジタル出力
- Grove インターフェース
- LEGO 互換ホール

## ドキュメント

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Light)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_light.png" height="300" width="300">
</figure>

## 関連情報

- [LIGHT ユニット](https://www.aliexpress.com/store/product/M5Stack-Official-Light-Unit-with-Photoresistance-Grove-Port-Analog-Digital-Output-Compatible-with-M5GO-FIRE-ESP32/3226069_32920589923.html)