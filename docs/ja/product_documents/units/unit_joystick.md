# JOYSTICK ユニット

## 概要

JOYSTICK ユニットはゲームコントローラーと同じジョイスティックです。X-Y軸オフセットとボタンクリックの入力を取得できます。

## 特徴

- LEGO 互換ホール
- Grove インターフェース

## ドキュメント

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Joystick)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_joystick.png">
</figure>

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_joystick_2.png">
</figure>

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_joystick_3.png">
</figure>

## 関連情報

- [JOYSTICK ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-New-Joystick-Unit-MEGA328P-I2C-Grove-Connector-Compatible-X-Y-Axis-Button-for-ESP32/3226069_32921785624.html)