# HUB ユニット

## 概要

このユニットは3つのGroveコネクタ備えたハブユニットです。ユーザは任意のユニットを3つまで追加する事ができます。

## 特徴

- 3つの4ピンGroveコネクタ
- LEGO 互換ホール

## ドキュメント

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_hub.png">
</figure>

## 関連情報

- [HUB ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-HUB-Unit-1-to-3-HUB-with-Universal-Connector-Grove-Port/3226069_32930928722.html)