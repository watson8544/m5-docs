# PROTO ユニット

## 概要

PROTO ユニットは2.54mmピッチのブランクホールが70個用意されています。

## 特徴

- フレキシブルなブランクホール
- 70 ホール (ピッチ: 2.54mm)
- Grove インターフェース
- LEGO 互換ホール

## ドキュメント

- GitHub
  - [M5GO](https://github.com/m5stack/M5GO)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_proto.jpg" height="300" width="300">
</figure>

## 関連情報

- [PROTO ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-Proto-Board-Unit-Universal-Double-Side-Prototype-2-54mm-PCB-Grove-Port-Compatible/3226069_32920617495.html)