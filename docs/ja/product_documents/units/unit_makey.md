# MAKEY ユニット

## 概要

MAKEY ユニットはATmega328Pを内蔵し、自由に使用可能な16のIOピンを備えています。I2Cでコントロール可能です。I2Cアドレスは**<mark>0x51</mark>**です。

## 特徴

- ATmega328P 内蔵
- 16 Keys Fruit Piano(PD0-7 & PB0-5)
- 1x NeoPixel pin(PC2)
- 1x Buzzer pin(PC3)
- ブザー内蔵
- Grove インターフェース
- LEGO 互換ホール

## アプリケーション

- Makeyアプリケーション
- Adafruitのライブラリを用いたRGB アプリケーション

## ドキュメント

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Makey)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_makey.png">
</figure>

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_makey_interface.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/units/unit_makey_application.jpg">
</figure>

## 関連情報

- [MAKEY ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Makey-Unit-MEGA328P-Inside-16Key-Fruit-Paino-with-NEO-Pixel-and-BUZZER-for-ESP32/3226069_32924883456.html)