# PIR ユニット

## 概要

PIR ユニットは人間の動き（モーション）を検出する事が可能です。

## 特徴

- 測定可能距離 150cm
- LEGO 互換ホール

## ドキュメント

- GitHub
  - [M5GO](https://github.com/m5stack/M5GO)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_pir.png">
</figure>

## 関連情報

- [PIR ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-PIR-Sensor-Human-Body-Infrared-PIR-Motion-Sensor-Detector-Module-GPIO-GROVE-Connector/3226069_32931794651.html)