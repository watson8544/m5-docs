# Dual BUTTON ユニット

## 概要

このユニットは2つのボタンが搭載されたユニットです。

## 特徴

- LEGO 互換ホール
- Grove インターフェース

## ドキュメント

- ウェブサイト
  - [M5Stack](https://www.m5stack.com)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_dual_button.png">
</figure>

## 関連情報

- [Dual BUTTON ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-New-Mini-Dual-Button-Unit-Mini-with-GROVE-Port-Cable-Connector-Compatible-with-FIRE/3226069_32923126250.html)