# NeoPixel ユニット

## 概要

NeoPixel ユニットは帯状にフルカラーLEDが連なったケーブルです。プログラマブルに色や点灯・点滅などをコントロールする事が可能です。Grove インターフェースで接続します。

## 特徴

- 測定可能距離: 10cm/20cm/0.5m/1m/2m
- Grove インターフェース
- LEGO 互換ホール

## ドキュメント

- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_neopixel.jpg" height="300" width="300">
</figure>

## 関連情報

- [NeoPixel ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-NeoPixel-RGB-LEDs-Cable-SK6812-with-GROVE-Port-2m-1m-50cm-20cm-10cm/3226069_32950831315.html)