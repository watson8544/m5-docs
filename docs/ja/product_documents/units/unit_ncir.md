# NCIR ユニット

## 概要

**NCIR ユニット**は内蔵されている**MLX90641**により赤外線を測定する事で、体温測定や動きを検出する事が可能です。I2C通信により値の取得が可能です。

## 特徴

- 高精度
- 検出範囲: -70℃~382.2℃
- LEGO 互換ホール

## アプリケーション

- 体温測定
- 動き検出

## ドキュメント

- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)
- データシート
  - [MLX90614](https://pdf1.alldatasheet.com/datasheet-pdf/view/218977/ETC2/MLX90614.html)
- 回路図
  - [Schematic](https://github.com/m5stack/M5Stack)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_ncir.png" height="300" width="300">
</figure>

## 関連情報

- [NCIR ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-NCIR-Unit-MLX90614-Contactless-Temperature-Sensor-Module-70C-382-2C-GROVE-I2C-Development-Board/3226069_32947772098.html)