# COLOR SENSOR ユニット

## 概要

COLOR SENSOR ユニットは内蔵されたTCS3472により、対象物表面の色を検出する事ができます。M5StackはI2Cで値を取得可能です。

## 特徴

- 高精度
- LEGO 互換ホール

## アプリケーション

- RGB LED バックライトコントロール
- 色校正

## ドキュメント

- GitHub
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Color)

- データシート
  - [TCS3472](https://pdf1.alldatasheet.com/datasheet-pdf/view/560511/AMSCO/TCS3472.html)

- 回路図
  - [Schematic](https://github.com/m5stack/M5Stack)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_color_sensor.png" height="300" width="300">
</figure>

## 関連情報

- [COLOR SENSOR ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Color-Unit-TCS34725-Color-Sensor-RGB-Color-Sensor-Development-Board-Module-GROVE-I2C-Compatible/3226069_32946957647.html)