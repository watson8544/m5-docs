# 3.96port ユニット

## 概要

3.96port ユニットはGroveからVH3.96-4ピンコネクタに変換するためのユニットです。

## 特徴

- VH3.96 インターフェース
- LEGO 互換ホール

## ドキュメント

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_3.96.jpg" height="300" width="300">
</figure>

## 関連情報

- [3.96port ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-3-96-4Pin-Transfer-Module-Grove-Compatible-with-M5GO-FIRE-ESP32-Development-Kit/3226069_32922623759.html)