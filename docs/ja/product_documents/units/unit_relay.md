# RELAY ユニット

## 概要

RELAY ユニットは大きな電力負荷を高い信頼性で安全にコントロールする事が可能です。最大 3A @ 24 VDC または 120 VACまでの負荷に対して対応しています。

## 特徴

- シングル入力
- 最大 3A @ 24 VDC または 120 VAC
- Grove インターフェース
- LEGO 互換ホール

## アプリケーション

- 大きな負荷の制御
- IoTアウトレット(壁のコンセント)

## ドキュメント

- GitHub
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Relay)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_relay.png" height="300" width="300">
</figure>

## 関連情報

- [RELAY ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-Relay-Unit-DC-3A-30V-AC-3A-220V-with-Triode-Driven-GROVE-Port/3226069_32922856211.html)