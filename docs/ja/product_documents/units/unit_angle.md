# ANGLE ユニット

## 概要

ANGLE ユニットは可変抵抗器です。回転角度検出や音量調節などに利用できます。

## 特徴

- LEGO 互換ホール

## ドキュメント

- GitHub
  - [M5GO](https://github.com/m5stack/M5GO)

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_angle.png">
</figure>

## 関連情報

- [ANGLE ユニット 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-Angle-Unit-Potentiometer-Inside-Resistance-Adjustable-GPIO-GROVE-Co-n-nec-to-r/3226069_32931834705.html)