# M5Stack FACES

## 概要

M5Stack FACES はESP32互換の３つの異なるキーボードのセットです。(キーボード、ゲームパッド、電卓キー)

M5Stack FACES を利用して、自作ゲームを作って遊んだり、リモコン電卓やリモートゲームコントローラーを作成することが可能です。
また別売りの[JOYSTICK](https://www.aliexpress.com/store/product/M5Stack-NEW-Joystick-Panel-for-M5-FACE-ESP32-Development-Kit-X-Y-Axis-Press-Button-with/3226069_32949801245.html)を利用する事も可能です。

## 特徴

- プログラミングサポート
  - Blockly
  - Arduino
  - MycroPython
- LEGO 互換ホール
- Nintendo エミュレータサポート
- [TFカード](https://ja.wikipedia.org/wiki/SD%E3%83%A1%E3%83%A2%E3%83%AA%E3%83%BC%E3%82%AB%E3%83%BC%E3%83%89)サポート

## パッケージ内容

- 1x M5Stack GRAY
- 1x キーボード
- 1x ゲームパッド
- 1x 電卓キー
- 1x FACE ボトム
- 1x CHG ベース
- 1x USB Type-C ケーブル
- ユーザーマニュアル

## アプリケーション

- 自作ゲーム
- Nintendo エミュレータ
- テトリス
- 自作電卓
- 自作ポケコン

## ドキュメント

- 回路図
  - [Schematic](https://github.com/m5stack/esp32-cam-demo/blob/m5cam/M5CAM-ESP32-A1-POWER.pdf)

- サンプルコード
  - [MicroPython](https://github.com/m5stack/M5GO/tree/master/examples)

- データシート
  - [ESP32](https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_cn.pdf)
  - [MPU9250](https://www.invensense.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf)

- GitHub
  - [Arduino](https://github.com/m5stack/M5Stack-nesemu)

## 関連情報

- [M5Stack FACES 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3649/)
- [M5Stack FACES 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-NEW-Offer-ESP32-Open-Source-Faces-Pocket-Computer-with-Keyboard-Gameboy-Calculator-for-Micropython-Arduino/3226069_32843973578.html)
- [M5Stack FACES JOYSTICK 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-NEW-Joystick-Panel-for-M5-FACE-ESP32-Development-Kit-X-Y-Axis-Press-Button-with/3226069_32949801245.html)