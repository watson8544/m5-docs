# M5Stack PLC

## 概要

M5Stack PLC はRS-485アダプタを含む工業向けベースです。
DC9V-24V入力、4ピンまたは6ピンのリレー出力、デジタル入力、通信インターフェースなどを備えます。

## 特徴

- 自由に自作可能
- プログラマブルロジックコントローラ (PLC)
- 軽量: 0.1kg (0.22lb.)
- パッケージサイズ: 5\*5\*5cm (1.97\*1.97\*1.97in)

## パッケージ内容

- 1x PLC-Protoボード
- 1x RS-485モジュール
- 1x PLCプラスチックエンクロージャ
- 1x スライドガイド
- 1x 磁石
- 1x 6ピン3.96ピッチ端子
- 1x 4ピン3.96ピッチ端子
- 3x 六角レンチ
- 7x 端子ピン
- 1x ステッカー

## アプリケーション

- プログラマブルロジックコントローラ
- プログラマブルモーションコントローラ
- デジタルオペレーションプロセッサ
- 強電コントローラ

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)
- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)

## 関連情報

- [M5Stack PLC 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3992/)
- [M5Stack PLC 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-PLC-Proto-Industrial-Board-Module-Contains-RS485-ACS712-5B-Programmable-Logic-Controller-Relay-with-Magnet/3226069_32874916056.html)
