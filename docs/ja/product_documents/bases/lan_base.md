# M5Stack LAN

## 概要

M5Stack LAN はI2Cとユーザ定義の6ピンポートによってイーサネット通信が可能なベースです。

M5Stackの無線通信が遅かったり、無線ネットワークに繋がらない時に、このLANベースを利用して、それらの厄介な問題を解決すルことができます。

## 特徴

- W5500 - 高性能イーサネットチップ

## パッケージ内容

- 1x M5Stack LAN ベース(ユーザ定義 6 PIN and 1 Groveポート)
- 1x DINレール
- 1x ステッカー

## アプリケーション

- ミニポータブルWEBサーバ
  - TCP/UDP サーバ
  - HTTP サーバ
- ミニポータブルWEBクライアント
  - TCP/UDP クライアント
  - HTTP クライアント
- SMTP/NTP サーバ

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Modules/W5500)

- データシート
  - [LAN](https://www.u-blox.com/zh/product/neo-m8-series)

- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)

## 関連情報

- [M5Stack LAN 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3994/)
- [M5Stack LAN 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-New-Arrival-LAN-Module-with-W5500-Chip-LanProto-Ethernet-convert-Network-Module-Microcontroller-for-Arduino/3226069_32904089417.html)
