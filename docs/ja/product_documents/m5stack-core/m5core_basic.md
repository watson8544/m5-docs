# M5Stack BASIC

## 概要

M5Stack **BASIC**は**ESP32**チップがベースです。**Blockly**、**Arduino**、**MicroPython**などでプログラミングすることができます。

M5Stack BASICにはESP32プログラミングに必要なものに加えて、TFT液晶も装備されています。簡易版"Leap Motion"のような3Dリモートジェスチャーコントローラなども短時間で作ることが出来るでしょう。

## 特徴

- プログラミングサポート
  - Arduino
  - Blockly
  - ESP-IDF
  - MicroPython
- [TFカード](https://ja.wikipedia.org/wiki/SD%E3%83%A1%E3%83%A2%E3%83%AA%E3%83%BC%E3%82%AB%E3%83%BC%E3%83%89)サポート

## スペック

|項目|詳細|
|:---|:---|
|Core| M5Stack BASIC |
|ESP32| 240MHz x 2コア, 600 DMIPS, 520K, Wi-Fi, デュアルモードBluetooth|
|Flash| 4M-Bytes |
|電源入力| 5V @ 500mA |
|インターフェース| USB Type-C x 1, Grove(I2C+I/0+UART) x 1|
|画面| 2 inch, 320x240 Colorful TFT LCD, ILI9342 |
|スピーカー| 1W-0928|
|電池| 150mAh @ 3.7V 内蔵|
|動作温度| 32°F to 104°F ( 0°C to 40°C ) |
|サイズ| 54 x 54 x 12.5 mm |
|ケース| プラスチック ( PC )|
|重量| 120g (ボトムモジュール含む）, 100g（コアのみ） |

## パッケージ内容

- 1x M5Stack BASIC
- 1x M5Stack BASIC ボトムモジュール
- USB Type-C ケーブル
- ユーザーマニュアル

## ドキュメント

- **回路図**
  - [Schematic](https://github.com/m5stack/M5-3D_and_PCB/blob/master/M5_Core_SCH%2820171206%29.pdf)

- **サンプルコード**
  - [Arduino Example](https://github.com/m5stack/M5Stack/tree/master/examples)

- **データシート**
  - [ESP32(中国語)](https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_cn.pdf)

- **GitHub**
  - [M5Stack](https://github.com/m5stack/M5Stack)

- **クイックスタート (Arduino)**
  - [macOS](/en/quick_start/m5core/m5stack_core_get_started_Arduino_MacOS)
  - [Windows](/en/quick_start/m5core/m5stack_core_get_started_Arduino_Windows)
  - [MicroPython](/en/quick_start/m5core/m5stack_core_get_started_MicroPython)

## 関連情報
  
- [M5Stack BASIC 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3647/)
- [M5Stack BASIC 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Stock-Offer-ESP32-Basic-Core-Development-Kit-Extensible-Micro-Control-Wifi-BLE-IoT-Prototype/3226069_32837164440.html)