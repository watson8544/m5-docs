# M5Stack GRAY

## 概要

M5Stack **GRAY**は**ESP32**チップがベースです。**Blockly**、**Arduino**、**MicroPython**などでプログラミングすることができます。

M5Stack GRAYはESP32プログラミングに必要なものに加えて、**MPU9250**MEMSセンサー（3軸ジャイロ + 3軸加速度 + 3軸地磁気)、TFT LCDなどを備えています。簡易版"Leap Motion"のような3Dリモートジェスチャーコントローラなどを短時間で作ることが出来るでしょう。

## 特徴

- プログラミングサポート
  - Arduino
  - ESP-IDF
  - MicroPython
- [TFカード](https://ja.wikipedia.org/wiki/SD%E3%83%A1%E3%83%A2%E3%83%AA%E3%83%BC%E3%82%AB%E3%83%BC%E3%83%89)サポート

## スペック

|項目|詳細|
|:---|:---|
|Core | M5Stack GRAY |
|ESP32| 240MHz x 2 コア, 600 DMIPS, 520K, Wi-Fi, デュアルモード Bluetooth|
|Flash| 4M-Bytes |
|電源入力| 5V @ 500mA|
|インターフェース| USB Type-C x 1, Grove(I2C+I/0+UART) x 1|
|画面| 2 inch, 320x240 Colorful TFT LCD, ILI9342 |
|スピーカー| 1W-0928 |
|MEMS| MPU9250 |
|電池| 150mAh @ 3.7V, inside 内蔵|
|動作温度| 32°F to 104°F ( 0°C to 40°C ) |
|サイズ| 54 x 54 x 12.5 mm |
|ケース| プラスチック ( PC ) |
|重量| 120g (ボトムモジュール含む), 100g (コアのみ)|

## パッケージ内容

- 1x M5Stack GRAY
- 1x M5Stack ボトムモジュール
- USB Type-C ケーブル
- ユーザーマニュアル

## ドキュメント

- **回路図**
  - [Schematic](https://github.com/m5stack/M5-3D_and_PCB/blob/master/M5_Core_SCH%2820171206%29.pdf)

- **サンプルコード**
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples)
  - [MicroPython](https://github.com/m5stack/M5GO/tree/master/examples)

- **データシート**
  - [ESP32](https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_cn.pdf)
  - [MPU9250](https://www.invensense.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf)

- **GitHub**
  - [M5Stack](https://github.com/m5stack/M5Stack)
  - [M5GO](https://github.com/m5stack/M5GO)

- **クイックスタート (Arduino)**
  - [macOS](/en/quick_start/m5core/m5stack_core_get_started_Arduino_MacOS)
  - [Windows](/en/quick_start/m5core/m5stack_core_get_started_Arduino_Windows)
  - [MicroPython](/en/quick_start/m5core/m5stack_core_get_started_MicroPython)

## 関連情報

- [M5Stack GRAY 購入 (スイッチサイエンス)](https://www.switch-science.com/catalog/3648/)
- [M5Stack GRAY 購入 (AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-In-Stock-ESP32-Mpu9250-9Axies-Motion-Sensor-Core-Development-Kit-Extensible-IoT-Development-Board/3226069_32836393710.html)