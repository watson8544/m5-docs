# M5Stack PLUS モジュール

## 概要

M5Stack PLUS モジュールは500mAhのバッテリ、ロータリエンコーダ、IR送信機、PORT B(UART)、PORT C(GPIO)そしてマイクジャックの為のパッドを備えています。M5Stackの下に重ねるだけで、機能を強化することが出来ます。M5Stackとの通信にはI2Cを使用します。I2Cアドレスは **<mark>0x89</mark>**です。

## 特徴

- 500mAh バッテリー
- プログラマブルロータリエンコーダ
- IR送信機
- PORT B (UART)
- PORT C (GPIO)

## パッケージ内容

- 1x PLUS モジュール

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)
- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Modules/Plus)
- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)

<figure>
    <img src="assets/img/product_pics/modules/plus_1.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/plus_2.png" height="300" width="300">
</figure>

## 関連情報

- [M5Stack PLUS モジュール 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-New-Arrival-PLUS-Module-Encoder-Module-with-MEGA328P-500mAh-Battery-ISP-IR-Transmitter-UART-GPIO/3226069_32949278724.html)