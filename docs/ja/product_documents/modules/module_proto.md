# M5Stack PROTO モジュール

## 概要

M5Stack PROTO モジュールは、各M5Stack Coreと接続可能な30ピンソケットとユニバーサル基板が一体になった試作用モジュールです。あなたのアイデアを自由に試作することができます。

## 特徴

- フレキシブルなブランクホール
- M5Stack Coreと互換性のある30ピンソケット
- 温湿度センサー DHT12 付属 (キット購入時)

## インターフェース

| LINE0            | LINE1            |
|:---|:---|
| GND              | IO35(ADC1)       |
| GND              | IO36(ADC2)       |
| GND              | EN               |
| IO23(MOSI)       | IO25(DAC0)       |
| IO19(MISO)       | IO26(DAC1)       |
| IO18(EXT_SCK)    | 3V3              |
| IO3(U1_RX)       | IO1(U1_TX)       |
| IO16(U1_RX)      | IO17(U2_TX)      |
| IO21(I2C_SDA)    | IO22(I2C_SCL)    |
| IO2              | IO5              |
| IO12(I2S_SCLK)   | IO13             |
| IO15(I2S_OUT)    | IO0              |
| HPOWR            | IO34             |
| HPOWR            | 5V               |
| HPOWR            | BAT              |

## パッケージ内容

### PROTO モジュール Kit

- 1x M5Stack PROTO モジュール
- 1x DHT12 温湿度センサー
- 1x バスソケット
- 1x Grove ケーブル
- 1x パッキングボックス
- ユーザーマニュアル

### PROTOT モジュール

- 1x M5Stack PROTO モジュール

### ボード

- 3x M5Stack PROTO ボード

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)

- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)

## 関連情報

- [PROTO モジュール Kit 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3651/)
- [PROTO モジュール 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3650/)
- [PROTO モジュール Kit 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Experimental-Proto-Board-Set-included-DHT12-Bus-Socke-Grove-Cable-for-ESP32-Basic-Kit/3226069_32841004439.html)
- [PROTO モジュール 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Stock-Offer-Proto-Module-Proto-Board-with-Extension-Bus-Socket-for-Arduino-ESP32-Development/3226069_32843231933.html)
- [PROTO ボード 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Core-Development-of-Experimental-Proto-Board-suitable-for-ESP32-Basic-Kit-and-Mpu9250-Kit/3226069_32837180999.html)
