# STEPMOTOR モジュール

## 概要

STEPMOTOR モジュールはATmega328Pを内蔵しており、GRBLファームウェアによってモータをコントロールすることが可能です。M5StackのCoreとの通信にはI2Cを利用しています。I2Cアドレスは**<mark>0x70</mark>**です。

## 特徴

- 電源入力 9-24V
- 3-Way STEPMOTOR<mark>(X, Y, Z)</mark>
- リチウムバッテリー用インターフェース内蔵

## パッケージ内容

- 1x STEPMOTOR モジュール
- 1x 12V 電源
- 1x 5V ファンモジュール

## アプリケーション

- 自作3Dプリンタ
- シンプルロボットアーム

## ドキュメント

- **サンプルコード**
  - [Arduino](https://github.com/m5stack/stepmotor_module/tree/master/StepMotor_M5test)

- **クイックスタート**
  - 近日公開...

<!--
<figure>
    <img src="assets/img/product_pics/modules/stepmotor_01.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_02.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_03.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_04.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_05.png" height="300" width="300">
</figure>
-->

## 関連情報

- [STEPMOTOR モジュール 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-New-Arrival-Stepmotor-Module-for-Arduino-ESP32-GRBL-12C-Step-Motor-MEGA328P-similar-as-12V/3226069_32889109142.html)