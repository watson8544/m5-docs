# M5Stack SIM800L モジュール

## 概要

M5Stack SIM800L モジュールは小型のSIM800Lを用いたGSM/GPRS通信用モジュールです。
M5Stackシリーズのコアを使用し、Blockly、Arduino、MicroPythonでプログラムすることが可能です。

SIM800LはクアッドバンドのGSM/GPRS通信ソリューションを提供します。SIM800LとM5StackはUSART2と呼ばれるシリアルポートで接続されます。もちろん、あなた自身でシリアルポート番号を変更することも可能です。

## 特徴

- SIM800L モジュール
- アンテナ内蔵
- 3.5 mm Phone Audio ジャック
- マイク
- パラメータ
- GSM/GPRS
- クアッドバンド 850/900/1800/1900MHz をサポート
- transmit Voice, SMS and data information with low power consumption
- 低消費電力で通話、SMS、データ通信が可能
- Bluetooth と Embedded AT

## パッケージ内容

- 1x M5Stack SIM800L モジュール

## アプリケーション

- 二酸化窒素アラーム
- 自動WebクローラーSMS通知
- リモート検針システム

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)
- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples)
- データシート
  - [SIM800L](http://simcomm2m.com/En/module/detail.aspx?id=138)(SIM800L)
- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)

## 関連情報

- [M5Stack SIM800L モジュール 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-In-Stock-GSM-Module-SIM800L-Stackable-IoT-Development-Board-for-Arduino-ESP32-with-MIC/3226069_32843211923.html)
