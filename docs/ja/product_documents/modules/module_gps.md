# M5Stack GPS モジュール

## 概要

M5Stack GPS　モジュールはu-bloxのNEO-M8Nを採用した小型のGPSモジュールです。

GPSモジュールはu-bloxのM8 GNSSエンジンにより、高い性能と受信感度を発揮します。

## 特徴

- GPS NEO-M8N モジュール
- 高性能
- 高受信感度 (–167 dBm)
- 最大 3 GNSS 同時受信可能
- 内蔵と外付けアンテナを選択可能

## パッケージ内容

- 1x M5Stack GPS モジュール
- 1x 外付けアンテナ

## アプリケーション

- 子供向けGPSブレスレット
- GPSによる物流トラッキングシステム

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)

- サンプルコード
  - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Modules/GPS)

- データシート
  - [GPS](https://www.u-blox.com/zh/product/neo-m8-series)

- GitHub
  - [M5Stack](https://github.com/m5stack/M5Stack)

## 関連情報

- [M5Stack GPS モジュール　購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3861/)
- [M5Stack GPS モジュール　購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-Stock-Offer-GPS-Module-with-Internal-External-Antenna-MCX-Interface-IoT-Development-Board-for/3226069_32840757048.html)