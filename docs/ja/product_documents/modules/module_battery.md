# M5Stack BATTERY モジュール

## 概要

M5Stack BATTERY モジュールは850mAhの大容量バッテリを内蔵しています。

これによりM5Stack単独で動作するポータブルデバイスを開発する事が可能です。

## 特徴

- 850mAh 大容量バッテリ

## パッケージ内容

- 1x M5Stack BATTERY モジュール

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)

## 関連情報

- [M5Stack BATTERY モジュール 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3653/)
- [M5Stack BATTERY モジュール 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-Official-In-Stock-Battery-Module-for-Arduino-ESP32-Core-Development-Kit-Capacity-850mAh-Stackable-IoT/3226069_32839688875.html)
