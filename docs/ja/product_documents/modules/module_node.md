# Node モジュール

## 概要

Node モジュールは12個のRGB LED(SK6812)、コーデックチップ(WM8978)、DHT12、IR送受信機(CHQ0038H)、2つのマイクを備えたサウンドボックスモジュールです。WM8978チップはHi-Fiスピーカーなどでよく使われています。

このモジュールを使えば、WEBラジオ、Bluetoothスピーカー、インテリジェントサウンドボックスなどを作ることが可能です。

## 特徴

- 12 x RGB LED
- 温湿度センサー DHT12
- Hi-Fiステレオコーデックチップ(最大分解能24bitDAC)
- IR送受信機内蔵
- マイク内蔵
- 3.5mm マイクジャック
- 内蔵リチウムバッテリー(500mAh)

## パッケージ内容

- 1x Node モジュール
- 1x ウォールホルダー
- 2x M3x12ネジ
- 2x M3x18ネジ
- 1x USB Type-C ケーブル

## アプリケーション

- WEBラジオ
- Bluetoothスピーカー
- インテリジェントサウンドボックス

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)
- データシート
  - [WM8978](http://pdf1.alldatasheet.com/datasheet-pdf/view/96647/WOLFSON/WM8978.html)
- 回路図
  - [Schematic](https://github.com/m5stack/M5StackModule-Node/tree/master/schematic)
- サンプルコード
  - [Arduino](https://github.com/m5stack/M5StackModule-Node/tree/master/example)

## 関連情報

- [Node モジュール 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-New-NODE-Samrt-Speaker-WM8978-Audio-Development-Board-I2S-Module-with-DHT12-Sensor-MIC-IR/3226069_32949773234.html)
