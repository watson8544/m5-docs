# M5Stack BTC モジュール

## 概要

M5Stack BTC モジュールは温度と湿度を計測することができるDHT12センサーを内蔵しています。
また充電スタンドとして利用できます。これにより小型温湿度計やBTCティッカーなどを作成する事が可能です。

## 特徴

- DHT12内蔵
- 充電スタンド

## パッケージ内容

- 1x BTC モジュール
- 1x USB Type-C ケーブル
- 2x M3x16ネジ
- 1x 六角レンチ

## ドキュメント

- ウェブサイト
  - [M5Stack](https://m5stack.com)

## 関連情報

- [M5Stack BTC モジュール 購入(スイッチサイエンス)](https://www.switch-science.com/catalog/3993/)
- [M5Stack BTC モジュール 購入(AliExpress)](https://www.aliexpress.com/store/product/M5Stack-New-BTC-Ticker-DHT12-Digital-Humidity-Temperature-Sensor-ESP32-for-Micropython-Bitcoin-Price-Ticker-with/3226069_32852302770.html)