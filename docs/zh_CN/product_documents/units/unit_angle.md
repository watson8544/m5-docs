# Unit ANGLE

## DESCRIPTION

ANGLE是一个电位器Unit，通过这个Unit可以检测手动旋转的角度.

## FEATURES

-  Unit上配置两个乐高安装孔

## DOCUMENTS

- **[GitHub]()**
- **[Purchase](https://www.aliexpress.com/store/3226069?spm=2114.search0104.3.5.66051a4dlpB2ti)**

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_angle.png">
</figure>