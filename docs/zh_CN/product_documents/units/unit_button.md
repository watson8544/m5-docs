# Unit BUTTON

## 描述

BUTTON是一个单按键unit，这个Unit能检测你是否按下了.

## 特性

-  Unit上配置两个乐高安装孔
-  GROVE接口

## 文档

- **[GitHub]()**
- **[Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-Mini-Button-Unit-for-ESP32-Arduino-Micropython-Development-Kit-with-GROVE-GPIO-Port-Blockly/3226069_32921805637.html?spm=a2g1x.12024536.productList_2187621.8)**

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_button.png">
</figure>