# Unit 3.96port

## 描述

3.96port是一个GROVE接口转VH3.96-4Pin接口的适配模块. 如果你想外接VH3.96-4Pin接口的设备的话，最好使用上这个Unit，会更加方便.

## 特性

-  VH3.96接口
-  Unit上配置两个乐高安装孔

## 文档

- **[原理图](https://github.com/m5stack/M5-Schematic/blob/master/Units/UNIT_2TO396.pdf)**
- **[旗舰店](https://www.aliexpress.com/store/3226069?spm=2114.search0104.3.5.66051a4dlpB2ti)**

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_3.96.jpg" height="300" width="300">
</figure>
