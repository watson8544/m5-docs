# Unit COLOR

## 描述

Color是一个颜色传感器. 通过GROVE接口(I2C)与M5Core相连，能够识别物体表面颜色，它内置了颜色传感器芯片TCS3472.

## 特性

-  高精度
-  检测的适用温度范围: -70℃~382.2℃
-  Unit上配置两个乐高安装孔

## 应用

-  RGB LED背光灯控制
-  产品颜色验证

## 文档

-  **GitHub** - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Color)

-  **Datasheet** - [TCS3472](https://pdf1.alldatasheet.com/datasheet-pdf/view/560511/AMSCO/TCS3472.html)

-  **[Schematic]()**

-  **[Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-Color-Unit-TCS34725-Color-Sensor-RGB-Color-Sensor-Development-Board-Module-GROVE-I2C-Compatible/3226069_32946957647.html?spm=a2g1x.12024536.productList_5885013.pic_5)**

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_color_sensor.png" height="300" width="300">
</figure>
