# Node

## 描述

<mark>Node</mark>是一个物联网情景中的节点/基站类底座。把Node安装到墙上，堆叠上任意一款M5Core主控，这时，Node+M5Core就是智能节点，可以与附近众多的节点或终端设备通信，可以用终端遥控器通过Node转发信号控制远程设备，实现多个智能终端互联。
* 内置12 RGBLed(SK6812)和温湿度传感器，可以显示多种丰富的Node状态，同时感知周围温湿度情况
* Node的四个角分别有一颗红外发送LED，底部有一颗红外接收管
* Node上下还有一对麦克风
* 内置常用于Hi-Fi耳机的高分辨率音频编解码芯片WM8978

你可以使用M5Core和Node底座实现智能家居中红外遥控设备，蓝牙wifi遥控设备的转发节点，因为内置高保真音频编解码芯片，甚至还可以实现智能音箱功能

## 特性

-  内置12颗RGBLed灯环和温湿度传感器
-  内置Hi-Fi级别的音频编解码芯片(高达24位的分辨率)
-  内置500mAh的小电池

## 包含

-  1x Node
-  Node的墙壁固定底座
-  4x 螺丝
-  1x Type-C USB线


## 应用

-  物联网中智能网络节点
-  网络收音机Webradio
-  智能音箱Intelligent sound box

## 文档

- **[官网](https://m5stack.com)**
- **[WM8978](http://pdf1.alldatasheet.com/datasheet-pdf/view/96647/WOLFSON/WM8978.html) (WM8978)**
- **[原理图](https://github.com/m5stack/M5StackModule-Node/tree/master/schematic)**
- **[例程](https://github.com/m5stack/M5StackModule-Node/tree/master/example)**
- **[快速上手]()**
- **[旗舰店](https://www.aliexpress.com/store/product/M5Stack-New-NODE-Samrt-Speaker-WM8978-Audio-Development-Board-I2S-Module-with-DHT12-Sensor-MIC-IR/3226069_32949773234.html)**

<figure>
    <img src="assets/img/product_pics/modules/node_01.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/node_02.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/node_03.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/node_04.jpg" height="300" width="300">
</figure>