# Step Motor Module

## DESCRIPTION

This is a <mark>StepMotor Driver Module</mark> built in MEGA328P MCU which has been burnt <mark>GRBL</mark> firmware used to control stepper motors. The module comunicates with M5Core via I2C. It's I2C address is 0x70.

## FEATURES

-  9-24V Power Input
-  Driver 3-way stepper motors<mark>(X, Y, Z)</mark>
-  Including a lithium battery interface

## INCLUDES

-  1x Step Motor Module
-  12V Power
-  1x 5V FAN Module for heat dissipation

## Applications

-  DIY 3D Printer
-  Simple Robot Arm

## DOCUMENTS

- **[Example](https://github.com/m5stack/stepmotor_module/tree/master/StepMotor_M5test)**
- **[Quick Start]()**
- **[Purchase](https://www.aliexpress.com/store/product/M5Stack-New-Arrival-Stepmotor-Module-for-Arduino-ESP32-GRBL-12C-Step-Motor-MEGA328P-similar-as-12V/3226069_32889109142.html?spm=2114.12010612.8148356.17.50511b9b5ViNuz)**

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_01.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_02.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_03.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_04.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/stepmotor_05.png" height="300" width="300">
</figure>