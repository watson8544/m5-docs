# M5Stack PLUS

## DESCRIPTION

THis a enhanced module including battery(500mAh), gear potentiometer, IR transmitter, PORT B(UART Port), PORT C(GPIO Port) and MicroPhone Pad. You can stack it with a M5Core for function enhancement. The module comunicates with M5Core with I2C. It's I2C address is 0x89.

## FEATURES

-  500mAh Battery
-  Programmable gear potentiometer
-  IR transmitter
-  Including PORT B and PORT C

## INCLUDES

-  1x PLUS Module

## DOCUMENTS

- [WebSite](https://m5stack.com)
- [Example]()
- [GitHub](https://github.com/m5stack/M5Stack)
- [Purchase](https://www.aliexpress.com/store/product/M5Stack-New-Arrival-PLUS-Module-Encoder-Module-with-MEGA328P-500mAh-Battery-ISP-IR-Transmitter-UART-GPIO/3226069_32949278724.html?spm=a2g1x.12024536.productList_5885013.pic_1)

<figure>
    <img src="assets/img/product_pics/modules/plus_1.png" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/plus_2.png" height="300" width="300">
</figure>