# 产品列表

中文 | [English](/en/product_documents) | [日本語](ja/product_documents)

<img src='assets/img/product_pics/1.jpg'> <img src='assets/img/product_pics/cores.png'>

| M5Core        | MiniCore      |
| :----------:  |:------------: |
| [BASIC](zh_CN/product_documents/m5stack-core/m5core_basic)         | [Stick](zh_CN/product_documents/m5stack-core/minicore_stick)         |
| [GRAY](zh_CN/product_documents/m5stack-core/m5core_gray)          | /            |
| [FIRE](zh_CN/product_documents/m5stack-core/m5core_fire)          | /            |



<img src='assets/img/product_pics/2.jpg'> <img src='assets/img/product_pics/module.png'>

| 无线通信模块      | 配件模块  | 控制模块   |
| :------------------:  |:------------------:| :--------------------:|
| [GPS](zh_CN/product_documents/modules/module_gps) | [PROTO](zh_CN/product_documents/modules/module_proto) | [STEPMOTOR](zh_CN/product_documents/modules/module_stepmotor)|
| [LORA](zh_CN/product_documents/modules/module_lora)                  | [BATTERY](zh_CN/product_documents/modules/module_battery)            | [SERVO](/zh_CN/product_documents/modules/module_servo)                     |
| /                   | [BTC](zh_CN/product_documents/modules/module_btc)                | /                     |
| [SIM800/GPRS/GSM](zh_CN/product_documents/modules/module_sim800)       | [PLUS](zh_CN/product_documents/modules/module_plus)                  | [COMMU](zh_CN/product_documents/modules/module_commu)                     |
| [LoRaWAN](/zh_CN/product_documents/modules/module_lorawan)                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |

<img src='assets/img/product_pics/5.jpg'> <img src='assets/img/product_pics/bases.png'>
- [M5GO底座](zh_CN/product_documents/bases/m5go_base)
- [PLC底座](zh_CN/product_documents/modules/module_plc)
- [FACE底座](zh_CN/product_documents/modules/module_face)
- [LAN底座](zh_CN/product_documents/bases/lan_base)
- [Node底座](zh_CN/product_documents/bases/node_base)

<img src='assets/img/product_pics/5.jpg'> <img src='assets/img/product_pics/accessory.png'>

- [LEGO-CABLE](zh_CN/product_documents/cables/accessory_lego_cable)
- [FRAME](zh_CN/product_documents/accessory_frame)



<img src='assets/img/product_pics/3.jpg'> <img src='assets/img/product_pics/unit.png'>

| 输入/传感类Unit   | 输出/控制类Unit  | 接口类Unit   |
| :-------------------: |:------------------------: | :----------------:|
| [ENV](zh_CN/product_documents/units/unit_env)                   | [RGB](zh_CN/product_documents/units/unit_rgb)                       | [HUB](zh_CN/product_documents/units/unit_hub)               |
| [IR](zh_CN/product_documents/units/unit_ir)                    | [RELAY](zh_CN/product_documents/units/unit_relay)                         | [3.96PORT](zh_CN/product_documents/units/unit_396port)          |
| [PIR](zh_CN/product_documents/units/unit_pir)                   | [NeoPixel](zh_CN/product_documents/units/unit_neopixel)                         | /                 |
| [ANGLE](zh_CN/product_documents/units/unit_angle)                   | /                         | /                 |
| [EARTH/Moisture](zh_CN/product_documents/units/unit_moisture)        | /                         | /                 |
| [LIGHT](zh_CN/product_documents/units/unit_light)                 | /                         | /                 |
| [MAKEY](zh_CN/product_documents/units/unit_makey)                   | /                         | /                 |
| [BUTTON](zh_CN/product_documents/units/unit_button)                   | /                         | /                 |
| [Dual-BUTTON](zh_CN/product_documents/units/unit_dual_button)                   | /                         | /                 |
| [JOYSTICK](zh_CN/product_documents/units/unit_joystick)                   | /                         | /                 |
| [THERMAL](zh_CN/product_documents/units/unit_thermal)                   | /                         | /                 |
| [ADC](zh_CN/product_documents/units/unit_ADC)                   | /                         | /                 |
| [DAC](zh_CN/product_documents/units/unit_DAC)                   | /                         | /                 |
| [Color Sensor](zh_CN/product_documents/units/unit_color_sensor)                   | /                         | /                 |
| [ToF](zh_CN/product_documents/units/unit_tof)                   | /                         | /                 |
| [NCIR](zh_CN/product_documents/units/unit_ncir)                           | /                         | /                 |


<img src='assets/img/product_pics/4.jpg'> <img src='assets/img/product_pics/application.png'>

- [BALA](zh_CN/product_documents/applications/application_bala)


<img src='assets/img/product_pics/6.jpg'> <img src='assets/img/product_pics/tool.png'>

- [M5Stack USB Downloader](zh_CN/product_documents/tools/tool_usb_downloader)
