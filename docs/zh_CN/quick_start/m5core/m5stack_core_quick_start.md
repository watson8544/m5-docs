# M5Stack Core Get Started

## Pick up your programming mode below for getting started

<img src="assets/img/getting_started_pics/arduino_logo.png"> | <img src="assets/img/getting_started_pics/blockly_and_micropython.png">
---|---
[Arduino](/en/quick_start/m5core/m5stack_core_get_started_Arduino_MacOS) | [Blockly/MicroPython](/en/quick_start/m5core/m5stack_core_get_started_MicroPython)
