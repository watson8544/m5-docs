# Product Documents

[中文](zh_CN/product_documents) | English | [日本語](ja/product_documents)

## Cores

<img src='assets/img/product_pics/1.jpg'> <img src='assets/img/product_pics/cores.png'>

| M5Core        | MiniCore      |
| :----------:  |:------------: |
| [BASIC](/en/product_documents/m5stack-core/m5core_basic)         | [Stick](/en/product_documents/m5stack-core/minicore_stick)         |
| [GRAY](/en/product_documents/m5stack-core/m5core_gray)          | /            |
| [FIRE](/en/product_documents/m5stack-core/m5core_fire)          | /            |
| [M5GO IOT Kit](/en/product_documents/m5stack-core/m5go_iot_starter_kit)          | /            |


## Modules

<img src='assets/img/product_pics/2.jpg'> <img src='assets/img/product_pics/module.png'>

| Wireless Modules      | Accessory Modules  | Controlling Modules   |
| :------------------:  |:------------------:| :--------------------:|
| [GPS](/en/product_documents/modules/module_gps) | [PROTO](/en/product_documents/modules/module_proto) | [STEPMOTOR](/en/product_documents/modules/module_stepmotor)|
| [LORA](/en/product_documents/modules/module_lora)                  | [BATTERY](/en/product_documents/modules/module_battery)            | [SERVO](/en/product_documents/modules/module_servo)                     |
| /                  | [BTC](/en/product_documents/modules/module_btc)                | /                    |
| [SIM800/GPRS/GSM](/en/product_documents/modules/module_sim800)       | [PLUS](/en/product_documents/modules/module_plus)                  | [COMMU](/en/product_documents/modules/module_commu)                    |
| [LoRaWAN](/en/product_documents/modules/module_lorawan)                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |
| /                     | /                  | /                     |

## Bases

<img src='assets/img/product_pics/5.jpg'> <img src='assets/img/product_pics/bases.png'>

- [M5GO-Base](/en/file_to_diplay_null)
- [PLC-Base](/en/product_documents/modules/module_plc)
- [FACE-Base](/en/product_documents/modules/module_face)
- [LAN](/en/product_documents/bases/lan_base)
- [Node-Base](/en/product_documents/bases/node_base)

## Accessories

<img src='assets/img/product_pics/5.jpg'> <img src='assets/img/product_pics/accessory.png'>

- [LEGO-CABLE](/en/product_documents/accessorries/cables/accessory_lego_cable)
- [FRAME](/en/product_documents/accessorries/accessory_frame)

## Units

<img src='assets/img/product_pics/3.jpg'> <img src='assets/img/product_pics/unit.png'>

| Input/Sensing Class   | Output/Controlling Class  | Interface Class   |
| :-------------------: |:------------------------: | :----------------:|
| [ENV](/en/product_documents/units/unit_env)                   | [RGB](/en/product_documents/units/unit_rgb)                       | [HUB](/en/product_documents/units/unit_hub)               |
| [IR](/en/product_documents/units/unit_ir)                    | [RELAY](/en/product_documents/units/unit_relay)                         | [3.96PORT](/en/product_documents/units/unit_396port)          |
| [PIR](/en/product_documents/units/unit_pir)                   | [NeoPixel](/en/product_documents/units/unit_neopixel)                         | /                 |
| [ANGLE](/en/product_documents/units/unit_angle)                   | /                         | /                  |
| [EARTH/Moisture](/en/product_documents/units/unit_moisture)        | /                         | /                 |
| [LIGHT](/en/product_documents/units/unit_light)                 | /                         | /                 |
| [MAKEY](/en/product_documents/units/unit_makey)                   | /                         | /                 |
| [BUTTON](/en/product_documents/units/unit_button)                   | /                         | /                 |
| [Dual-BUTTON](/en/product_documents/units/unit_dual_button)                   | /                         | /                 |
| [JOYSTICK](/en/product_documents/units/unit_joystick)                   | /                         | /                 |
| [THERMAL](/en/product_documents/units/unit_thermal)                   | /                         | /                 |
| [ADC](/en/product_documents/units/unit_ADC)                   | /                         | /                 |
| [DAC](/en/product_documents/units/unit_DAC)                   | /                         | /                 |
| [Color Sensor](/en/product_documents/units/unit_color_sensor)                   | /                         | /                 |
| [ToF](/en/product_documents/units/unit_tof)                   | /                         | /                 |
| [ESP32Cam](/en/product_documents/units/unit_esp32cam)                   | /                         | /                 |
| [M5Camera](/en/product_documents/units/unit_m5camera)                   | /                         | /                 |
| [NCIR](/en/product_documents/units/unit_ncir)                           | /                         | /                 |

## Applications

<img src='assets/img/product_pics/4.jpg'> <img src='assets/img/product_pics/application.png'>

- [BALA](/en/product_documents/applications/application_bala)

## Tools

<img src='assets/img/product_pics/6.jpg'> <img src='assets/img/product_pics/tool.png'>

- [M5Stack USB Downloader](/en/product_documents/tools/tool_usb_downloader)
