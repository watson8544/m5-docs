# SERVO

## DESCRIPTION

Servo is a servo-motor driver module which can drive 12-way servo motors simultaneously. But actually, in our experiment, because of the maximum current, the maximum number of servo motor it can drive is 9. So we will publish a upgraded version soon after.

It's too easy to drive servo motors so that you can drive many servo motors after programmed 2-3 lines code in Arduino IDE or dragged 2-3 block in M5Flow.

Servo is communicated with M5Core through GROVE interface(I2C communication). The I2C address is 0x53.

## FEATURES

-  Drive multi-way servo motor simultaneously
-  Support wide-voltage power input: 6-12V
-  Support programming with Arduino IDE or M5Flow

## INCLUDES

-  1x M5Stack Servo Module
-  1x 6-12V Power Interface

## Applications

-  Humanoid robot
-  Bionic multi-joint robot
-  3-axis head for camera

## DOCUMENTS

-  **[Schematic](en/file_to_display_null)**

-  **GitHub** - [Arduino GitHub](en/file_to_display_null)

-  **Example** - [Arduino Example](en/file_to_display_null)

- **[Purchase](https://www.aliexpress.com/store/product/M5Stack-New-SERVO-Module-Board-12-Channels-Servo-Controller-with-MEGA328-Inside-Power-Adapter-6-24V/3226069_32951356502.html?spm=a2g1y.12024536.productList_5885011.pic_0)**

<figure>
    <img src="assets/img/product_pics/modules/servo_01.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/servo_02.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/servo_03.jpg" height="300" width="300">
</figure>