# LoRaWAN 

## DESCRIPTION

<mark>LoRaWAN</mark> is a small LoRa terminal module built-in LoRa chip(SX1276) and ST MCU that means this module has been built with complete LoRa protocal stack. So you can develop a LoRaWAN module through UART or simple AT command with M5Core.

By default, the UART configuration: "9600, 8, n, 1"(8 bits data, no parity, 1 stop bit)

?> **Notice** The 5 holes which are under the silk screen "LoRaWAN" are designed for upgrading the firmware of LoRaWAN module.

## FEATURES

-  Supports 433/470MHz and 868/915MHz
-  Supports DataRate: 0.018-38.4kbps
-  Output Power: 17 ± 0.5dbm
-  Supports ADR(Adaptive Data Rate)
-  Build-in Antenna

## INCLUDES

-  1x LoRa Module

## Applications

-  Automatic meter reading
-  Home building automation
-  Remote irrigation system

## PinMap

| *RHF76-052_UART* | *ESP32 Chip* |
| :----------: |:------------: |
| RXD       | GPIO16    |
| TXD      | GPIO17     |

## DOCUMENTS

- **[Example](en/file_to_display_null)**
- **[LoRaWAN Info](http://wiki.ai-thinker.com/sx127x-052) (LoRaWAN)**
- **[AT command for LoRaWAN](http://wiki.ai-thinker.com/_media/rhf-ps01509_lorawan_class_ac_at_command_specification_-_v4.4.pdf)**
- **[Purchase](https://www.aliexpress.com/store/product/M5Stack-New-LoRaWAN-Module-433-470Mhz-868-915MHz-with-Internal-Antenna-and-MCX-External-Antenna-Port/3226069_32953098569.html?spm=a2g1y.12024536.productList_5885011.pic_2)**

<figure>
    <img src="assets/img/product_pics/modules/LoRaWAN_01.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/LoRaWAN_02.jpg" height="300" width="300">
</figure>