# Unit COLOR

## DESCRIPTION

This is a unit can detecte the color of object surface which integrates TCS3472 (a color sensor). The unit comunicates with M5Core with I2C.

## FEATURES

-  High precision
-  Detection range: -70℃~382.2℃
-  Two Lego installation holes

## APPLICATION

-  RGB LED Backlight Control
-  Product Color Verification

## DOCUMENTS

-  **GitHub** - [Arduino](https://github.com/m5stack/M5Stack/tree/master/examples/Unit/Color)

-  **Datasheet** - [TCS3472](https://pdf1.alldatasheet.com/datasheet-pdf/view/560511/AMSCO/TCS3472.html)

-  **[Schematic](en/file_to_display_null)**

-  **[Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-Color-Unit-TCS34725-Color-Sensor-RGB-Color-Sensor-Development-Board-Module-GROVE-I2C-Compatible/3226069_32946957647.html?spm=a2g1x.12024536.productList_5885013.pic_5)**

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_color_sensor.png" height="300" width="300">
</figure>
