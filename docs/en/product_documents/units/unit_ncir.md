# Unit NCIR

## DESCRIPTION

This is a unit can measure body temperature or be applicated for movement detection which integrates MLX90641 (an infrared thermometer designed for non-contact temperature sensing). The unit comunicates with M5Core with I2C.

## FEATURES

-  High precision
-  Detection range: -70℃~382.2℃
-  Two Lego installation holes

## APPLICATION

-  body temperature measurement
-  movement detection

## DOCUMENTS

-  **GitHub** - [Arduino](en/file_to_display_null)

-  **Datasheet** - [MLX90614](https://pdf1.alldatasheet.com/datasheet-pdf/view/218977/ETC2/MLX90614.html)

-  **[Schematic](en/file_to_display_null)**

-  **[Purchase](https://www.aliexpress.com/store/product/M5Stack-Official-NCIR-Unit-MLX90614-Contactless-Temperature-Sensor-Module-70C-382-2C-GROVE-I2C-Development-Board/3226069_32947772098.html?spm=a2g1x.12024536.productList_5885013.pic_4)**

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_ncir.png" height="300" width="300">
</figure>
