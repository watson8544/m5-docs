# Unit 3.96port

## DESCRIPTION

The Unit 3.96port is a unit that aim to transfer GROVE interface to VH3.96-4Pin.

## FEATURES

-  VH3.96 interface
-  Two Lego installation holes

## DOCUMENTS

- **[Schematic](https://github.com/m5stack/M5-Schematic/blob/master/Units/UNIT_2TO396.pdf)**

- **[Purchase](https://www.aliexpress.com/store/3226069?spm=2114.search0104.3.5.66051a4dlpB2ti)**

<figure>
    <img src="assets/img/product_pics/units/M5GO_Unit_3.96.jpg" height="300" width="300">
</figure>
