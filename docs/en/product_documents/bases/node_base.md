# Node Module

## DESCRIPTION

<mark>Node</mark> is a base as a intelligent node in the IOT. If stacked a M5Core, it will be a intelligent node that can communicate with surrounding equipments via IR, Bluetooth or WIFI.

* Including 12 RGBs and one temperature and humidity sensor(DHT12), it means <mark>Node</mark> can display its own status and perception surrounding environment.
* There are 4 IR Transmitter LED at four corners and two IR Receiver
* There are two MIC
* A Codec chip(WM8978) inside that is often used to be applied for Hi-Fi Speaker.

## FEATURES

-  Including 12 RGBLed
-  Including a HiFi stereo codec chip(Up to 24bit DAC)
-  Including a lithium battery interface

## INCLUDES

-  1x Node Module
-  Wall holder
-  4x screws
-  4x Type-C USB Cable


## Applications

-  Intelligent IOT node
-  Webradio
-  Intelligent sound box

## DOCUMENTS

- **[WebSite](https://m5stack.com)**
- **[WM8978](http://pdf1.alldatasheet.com/datasheet-pdf/view/96647/WOLFSON/WM8978.html) (WM8978)**
- **[Schematic](https://github.com/m5stack/M5StackModule-Node/tree/master/schematic)**
- **[Example](https://github.com/m5stack/M5StackModule-Node/tree/master/example)**
- **[Quick Start]()**
- **[Purchase](https://www.aliexpress.com/store/product/M5Stack-New-NODE-Samrt-Speaker-WM8978-Audio-Development-Board-I2S-Module-with-DHT12-Sensor-MIC-IR/3226069_32949773234.html)**

<figure>
    <img src="assets/img/product_pics/modules/node_01.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/node_02.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/node_03.jpg" height="300" width="300">
</figure>

<figure>
    <img src="assets/img/product_pics/modules/node_04.jpg" height="300" width="300">
</figure>